package co.com.pragma.client;



import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


import co.com.pragma.model.Image;

@Component
public class ImageHystrixFallbackFactory implements IImageClient {
	@Override
	public ResponseEntity<Image> getImageByIdentificationNumber(String identificationNumber) {
		return ResponseEntity.ok(Image.builder().identificationNumber("none").image("none").build());
	}

	@Override
	public ResponseEntity<Void> createImageBody(Image images) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Image> updateImageBody(String id, Image imagesr) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Image> deleteImage(String id) {
		// TODO Auto-generated method stub
		return null;
	}



}
