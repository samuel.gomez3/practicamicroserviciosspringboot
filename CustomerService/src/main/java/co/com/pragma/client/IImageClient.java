package co.com.pragma.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.com.pragma.model.Image;

@FeignClient(name = "ImageService", fallback = ImageHystrixFallbackFactory.class)
public interface IImageClient {

	@RequestMapping(value = "/images/identificationNumber/{identificationNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Image> getImageByIdentificationNumber(
			@PathVariable("identificationNumber") String identificationNumber);

	@RequestMapping(value = "/images/body", method = RequestMethod.POST)
	public ResponseEntity<Void> createImageBody(@RequestBody Image images);

	@RequestMapping(value = "/images/body/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Image> updateImageBody(@PathVariable("id") String id, @RequestBody Image images);

	@RequestMapping(value = "/images/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Image> deleteImage(@PathVariable("id") String id);

}
