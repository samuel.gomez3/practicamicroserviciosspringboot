package co.com.pragma.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import co.com.pragma.model.Image;
import lombok.Data;

@Entity
@Data
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Size(min = 2, max = 60, message = "The size of the string <<names>> must be between 2 and 60 characters.")
	private String names;
	@Size(min = 2, max = 60, message = "The size of the string <<surnames>> must be between 2 and 60 characters.")
	private String surnames;
	@Size(min = 1, max = 3, message = "The size of the string <<typeIdentification>> must be between 1 and 3 characters.")
	private String typeIdentification;
	@Size(min = 8, max = 12, message = "The size of the string <<identificationNumber>> must be between 8 and 12 characters.")
	private String identificationNumber;
	@Min(13)
	private int age;
	@Size(min = 2, max = 60, message = "The size of the string <<cityBirth>> must be between 2 and 60 characters.")
	private String cityBirth;
	@Transient
	private Image image;

}
