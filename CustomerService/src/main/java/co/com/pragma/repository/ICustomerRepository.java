package co.com.pragma.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import co.com.pragma.entity.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Long>,  JpaSpecificationExecutor<Customer> {

	@Query(nativeQuery = false,value = " SELECT p FROM Customer p WHERE identificationNumber = ?1 and typeIdentification = ?2")
	List<Customer> findByIdentificationNumber(String identificationNumber, String typeIdentification);
	
	@Query(nativeQuery = false,value = " SELECT p FROM Customer p WHERE age > ?1")
	List<Customer> findByAge(int age);

}
