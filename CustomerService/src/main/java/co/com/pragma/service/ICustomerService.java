package co.com.pragma.service;

import java.util.List;

import co.com.pragma.entity.Customer;

public interface ICustomerService {
	
	Customer findCustomerById(long id);

	Customer findCustomerByIdentificationNumber(String identificationNumber);

	void saveCustomer(Customer customer);

	void updateCustomer(Customer customer);

	void deleteCustomer(long id);

	List<Customer> findAllCustomers();

	boolean isExist(Customer customer);

	List<Customer> findCustomersByAge(int age);

	Customer findCustomerByIdWithImage(long id);

	Customer findCustomerByIdentificationNumberWithImage(String identificationNumber);

	void saveCustomerWithImage(Customer customer);

	void updateCustomerWithImage(Customer customer, String idImage);

	void deleteCustomerWithImage(long id);

	List<Customer> findAllCustomersWithImage();

	List<Customer> findCustomersByAgeWithImage(int age);

}
