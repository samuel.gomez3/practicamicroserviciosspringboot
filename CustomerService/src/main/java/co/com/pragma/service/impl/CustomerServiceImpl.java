package co.com.pragma.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.pragma.client.IImageClient;
import co.com.pragma.entity.Customer;
import co.com.pragma.repository.ICustomerRepository;
import co.com.pragma.service.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	private ICustomerRepository customerRepository;

	@Autowired
	private IImageClient iImageClient;

	@Override
	public Customer findCustomerById(long id) {
		return (Customer) customerRepository.findById(id).orElse(null);
	}

	@Override
	public Customer findCustomerByIdWithImage(long id) {
		Customer customer = (Customer) customerRepository.findById(id).orElse(null);
		if (customer != null)
			customer.setImage(
					iImageClient.getImageByIdentificationNumber(customer.getIdentificationNumber()).getBody());
		return customer;
	}

	@Override
	public Customer findCustomerByIdentificationNumber(String identificationNumber) {
		List<Customer> customers = customerRepository
				.findByIdentificationNumber(identificationNumber.substring(2), identificationNumber.substring(0, 2))
				.stream().map(customer -> {
					customer.setImage(
							iImageClient.getImageByIdentificationNumber(customer.getIdentificationNumber()).getBody());
					return customer;
				}).collect(Collectors.toList());

		if (customers.size() == 0)
			return null;
		else
			return customers.get(0);
	}

	@Override
	public Customer findCustomerByIdentificationNumberWithImage(String identificationNumber) {
		List<Customer> customers = customerRepository.findByIdentificationNumber(identificationNumber.substring(2),
				identificationNumber.substring(0, 2));
		if (customers.size() == 0)
			return null;
		else
			return customers.get(0);
	}

	@Override
	public void saveCustomer(Customer customer) {
		customerRepository.save(customer);
	}

	@Override
	public void saveCustomerWithImage(Customer customer) {
		customerRepository.save(customer);
		iImageClient.createImageBody(customer.getImage());
	}

	@Override
	public void updateCustomer(Customer customer) {
		customerRepository.save(customer);
	}

	@Override
	public void updateCustomerWithImage(Customer customer, String idImage) {
		customerRepository.save(customer);
		iImageClient.updateImageBody(idImage, customer.getImage());
	}

	@Override
	public void deleteCustomer(long id) {
		customerRepository.deleteById(id);
	}

	@Override
	public void deleteCustomerWithImage(long id) {
		iImageClient.deleteImage(findCustomerByIdWithImage(id).getImage().getId());
		customerRepository.deleteById(id);
	}

	@Override
	public List<Customer> findAllCustomers() {
		return ((List<Customer>) customerRepository.findAll());
	}

	@Override
	public List<Customer> findAllCustomersWithImage() {
		return ((List<Customer>) customerRepository.findAll()).stream().map(customer -> {
			customer.setImage(
					iImageClient.getImageByIdentificationNumber(customer.getIdentificationNumber()).getBody());
			return customer;
		}).collect(Collectors.toList());
	}

	@Override
	public boolean isExist(Customer customer) {
		return findCustomerByIdentificationNumber(customer.getIdentificationNumber()) != null;
	}

	@Override
	public List<Customer> findCustomersByAge(int age) {
		return customerRepository.findByAge(age);
	}

	@Override
	public List<Customer> findCustomersByAgeWithImage(int age) {
		return customerRepository.findByAge(age).stream().map(customer -> {
			customer.setImage(
					iImageClient.getImageByIdentificationNumber(customer.getIdentificationNumber()).getBody());
			return customer;
		}).collect(Collectors.toList());
	}

}
