package co.com.pragma.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import co.com.pragma.entity.Image;


@Repository
public interface IImageRepository extends MongoRepository<Image, String> {
	public Image findByIdentificationNumber(String identificationNumber);

}
