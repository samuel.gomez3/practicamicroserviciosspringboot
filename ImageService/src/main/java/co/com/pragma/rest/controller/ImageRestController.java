package co.com.pragma.rest.controller;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import co.com.pragma.entity.Image;
import co.com.pragma.service.IImageService;

@RestController
@RequestMapping("/images")
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
public class ImageRestController {

	@Autowired
	private IImageService iImageService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Image>> listAllImages() {
		List<Image> images = iImageService.findAllImages();
		if (images.isEmpty())
			return new ResponseEntity<List<Image>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Image>>(images, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Image> getImageById(@PathVariable("id") String id) {
		Image images = iImageService.findImageById(id);
		if (images == null)
			return new ResponseEntity<Image>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Image>(images, HttpStatus.OK);
	}

	@RequestMapping(value = "/identificationNumber/{identificationNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Image> getImageByIdentificationNumber(
			@PathVariable("identificationNumber") String identificationNumber) {
		Image images = iImageService.findImageByIdentificationNumber(identificationNumber);
		if (images == null)
			return new ResponseEntity<Image>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Image>(images, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> createImage(@RequestParam("image") MultipartFile image,
			@RequestParam("identificationNumber") String identificationNumber, HttpServletRequest request,
			UriComponentsBuilder uriComponentsBuilder) {
		Image images = new Image();
		try {
			images.setImage(Base64.getEncoder().encodeToString(image.getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		images.setIdentificationNumber(identificationNumber);
		if (iImageService.isExist(images))
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		iImageService.saveImage(images);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/{id}").buildAndExpand(images.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/body", method = RequestMethod.POST)
	public ResponseEntity<Void> createImageBody(@RequestBody Image images, HttpServletRequest request,
			UriComponentsBuilder uriComponentsBuilder) {
		if (iImageService.isExist(images))
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		iImageService.saveImage(images);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/{id}").buildAndExpand(images.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Image> updateImage(@PathVariable("id") String id, @RequestParam("image") MultipartFile image,
			@RequestParam("identificationNumber") String identificationNumber, HttpServletRequest request) {
		Image currentImage = iImageService.findImageById(id);
		if (currentImage == null)
			return new ResponseEntity<Image>(HttpStatus.NOT_FOUND);
		currentImage.setIdentificationNumber(identificationNumber);
		try {
			currentImage.setImage(Base64.getEncoder().encodeToString(image.getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		iImageService.updateImage(currentImage);
		return new ResponseEntity<Image>(currentImage, HttpStatus.OK);
	}

	@RequestMapping(value = "/body/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Image> updateImageBody(@PathVariable("id") String id, @RequestBody Image images,
			HttpServletRequest request, UriComponentsBuilder uriComponentsBuilder) {
		Image currentImage = iImageService.findImageById(id);
		if (currentImage == null)
			return new ResponseEntity<Image>(HttpStatus.NOT_FOUND);
		currentImage.setIdentificationNumber(images.getIdentificationNumber());
		currentImage.setImage(images.getImage());
		iImageService.updateImage(currentImage);
		return new ResponseEntity<Image>(currentImage, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Image> deleteImage(@PathVariable("id") String id) {
		Image cliente = iImageService.findImageById(id);
		if (cliente == null)
			return new ResponseEntity<Image>(HttpStatus.NOT_FOUND);
		iImageService.deleteImage(id);
		return new ResponseEntity<Image>(HttpStatus.NO_CONTENT);
	}

}
