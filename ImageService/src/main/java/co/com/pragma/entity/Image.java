package co.com.pragma.entity;

import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "Image")
@Data
public class Image {
	@Id
	private String id;
	private String identificationNumber;
	private String image;

}
