package co.com.pragma.service;

import java.util.List;

import co.com.pragma.entity.Image;

public interface IImageService {

	Image saveImage(Image image);

	Image updateImage(Image image);

	void deleteImage(String id);

	Image findImageById(String id);

	List<Image> findAllImages();

	Image findImageByIdentificationNumber(String identificationNumber);

	public boolean isExist(Image image);

}
